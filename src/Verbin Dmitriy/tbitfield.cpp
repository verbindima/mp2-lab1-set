// ННГУ, ВМК, Курс "Методы программирования-2", С++, ООП
//
// tbitfield.cpp - Copyright (c) Гергель В.П. 07.05.2001
//   Переработано для Microsoft Visual Studio 2008 Сысоевым А.В. (19.04.2015)
//
// Битовое поле

#include "tbitfield.h"
#include "math.h"

#define BYTE 8

inline int GetShift()
{
	int size = sizeof(TELEM) * BYTE;
	int count = 0;
	while (size >>= 1) count++;
	return count;
}

TBitField::TBitField(int len)
{
	if (len <= 0)
		throw "размер поля должен быть положительным числом";
	BitLen = len;
	MemLen = GetMemLenght(len);
	pMem = new TELEM[MemLen];
	for (int i = 0; i < MemLen; i++)
		pMem[i] = 0;
}

TBitField::TBitField(const TBitField &bf) // конструктор копирования
{
	BitLen = bf.GetLength();
	MemLen = GetMemLenght(BitLen);
	pMem = new TELEM[MemLen];
	for (int i = 0; i < bf.GetLength(); i++)
	{
		if (bf.GetBit(i))
			SetBit(i);
		else
			ClrBit(i);
	}
}

TBitField::~TBitField()
{
	delete[] pMem;
}

int TBitField::GetMemLenght(const int len)
{
	return (len + sizeof(TELEM)*BYTE - 1) >> GetShift();
}


int TBitField::GetMemIndex(const int n) const // индекс Мем для бита n
{
	if (n >= BitLen || n < 0)
		throw "бит n выходит за границы битового поля";
	return n >> GetShift();
}

TELEM TBitField::GetMemMask(const int n) const // битовая маска для бита n
{
	if (n >= BitLen || n < 0)
		throw "бит n выходит за границы битового поля";
	return 1 << (n - GetMemIndex(n)*sizeof(TELEM)*BYTE);
}

// доступ к битам битового поля

int TBitField::GetLength(void) const // получить длину (к-во битов)
{
	return BitLen;
}

void TBitField::SetBit(const int n) // установить бит
{
	if (n >= BitLen || n < 0)
		throw "бит n выходит за границы битового поля";
	pMem[GetMemIndex(n)] |= GetMemMask(n);
}

void TBitField::ClrBit(const int n) // очистить бит
{
	if (n >= BitLen || n < 0)
		throw "бит n выходит за границы битового поля";
	pMem[GetMemIndex(n)] &= ~GetMemMask(n);
}

int TBitField::GetBit(const int n) const // получить значение бита
{
	if (n >= BitLen || n < 0)
		throw "бит n выходит за границы битового поля";
	return pMem[GetMemIndex(n)] & GetMemMask(n);
}

// битовые операции

TBitField& TBitField::operator=(const TBitField &bf) // присваивание
{
	if (pMem == NULL) throw "Ошибка. Объект не существует";
	delete[] pMem;
	BitLen = bf.GetLength();
	MemLen = GetMemLenght(BitLen);
	pMem = new TELEM[MemLen];
	for (int i = 0; i < MemLen; i++)
		pMem[i] = bf.pMem[i];
	return *this;
}

int TBitField::operator==(const TBitField &bf) const // сравнение
{
	if (GetLength() == bf.GetLength())
	{
		for (int i = 0; i < BitLen; i++)
		{
			if (GetBit(i) != bf.GetBit(i))
				return 0;
		}
	}
	else
		return 0;
	return 1;
}

int TBitField::operator!=(const TBitField &bf) const // сравнение
{
	return !operator==(bf);
}

TBitField TBitField::operator|(const TBitField &bf) // операция "или"
{
	TBitField tempBitField((BitLen > bf.BitLen) ? BitLen : bf.BitLen);
	for (int i = 0; i < MemLen; i++)
		tempBitField.pMem[i] = pMem[i];
	for (int i = 0; i < bf.MemLen; i++)
		tempBitField.pMem[i] = tempBitField.pMem[i] | bf.pMem[i];
	return tempBitField;
}

TBitField TBitField::operator&(const TBitField &bf) // операция "и"
{
	TBitField tempBitField((BitLen > bf.BitLen) ? BitLen : bf.BitLen);
	for (int i = 0; i < MemLen; i++)
		tempBitField.pMem[i] = pMem[i];
	for (int i = 0; i < bf.MemLen; i++)
		tempBitField.pMem[i] = tempBitField.pMem[i] & bf.pMem[i];
	return tempBitField;
}

TBitField TBitField::operator~(void) // отрицание
{
	TBitField tempBitField(GetLength());
	for (int i = 0; i < BitLen; i++)
	{
		if (!GetBit(i)) tempBitField.SetBit(i);
		else tempBitField.ClrBit(i);
	}
	return tempBitField;
}

// ввод/вывод

istream &operator>>(istream &istr, TBitField &bf) // ввод
{
	char ch;
	int i = 0;
	while (true)
	{
		istr >> ch;
		if (i < bf.GetLength())
		{
			if (ch == '1')
				bf.SetBit(i);
			else
				if (ch == '0')
					bf.ClrBit(i);
				else
					break;
		}
		else
			break;
		i++;
	}
	return istr;
}

ostream &operator<<(ostream &ostr, const TBitField &bf) // вывод
{
	for (int i = 0; i < bf.GetLength(); i++)
		if (bf.GetBit(i))
			ostr << 1;
		else
			ostr << 0;
	return ostr;
}