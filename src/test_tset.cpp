TEST(TSet, can_combine_three_sets_of_non_equal_size)
{
	const int size1 = 3, size2 = 5, size3 = 7;
	TSet set1(size1), set2(size2), set3(size3), expSet(size3);
	
	set1.InsElem(1);
	set1.InsElem(2);
	
	set2.InsElem(0);
	set2.InsElem(1);
	set2.InsElem(2);
	
	set3.InsElem(5);
	set3.InsElem(6);
	
	expSet.InsElem(0);
	expSet.InsElem(1);
	expSet.InsElem(2);
	expSet.InsElem(5);
	expSet.InsElem(6);
	
	EXPECT_EQ(expSet, set1 + set2 + set3);
}
